#Header files
HEADERS += hiddevice.h \
	hiddevices.h \
	hiddevicethread.h \
	hidkbdevice.h \
	hidlcddevice.h \
	hidparser.h \
	hidtypes.h

#Source files
SOURCES += hiddevice.cpp \
	hiddevices.cpp \
	hiddevicethread.cpp \
	hidkbdevice.cpp \
	hidlcddevice.cpp \
	hidparser.cpp \
	hidtypes.cpp

#Forms
#FORMS += 

#Resource file(s)
#RESOURCES +=

